create database running_state;

create table running_state.algorithm_config
(
    id              UUID,
    dept_no         String,
    alarm_type      String,
    reduction       Int32,
    reduction_times Int32,
    reduction_rate  Float,
    recover_period  Int32
) engine = ReplacingMergeTree
      PRIMARY key id
;
insert into running_state.algorithm_config(*)
values (generateUUIDv4(), '00', 1000126, 10, 1, 3, 15),
       (generateUUIDv4(), '00', 1000007, 10, 1, 3, 15),
       (generateUUIDv4(), '00', 1000154, 3, 1, 2, 3),
       (generateUUIDv4(), '00', 1000023, 8, 1, 3, 4),
       (generateUUIDv4(), '00', 9000006, 3, 1, 3, 3);

create table running_state.running_state_detail_sub
(
    record_date   Date,
    lift_no       String,
    alarm_type    String,
    alarm_num     Int32,
    duration_days Int8,
    scores        Decimal32(2),
    state_id      String,
    tsid          Decimal32(2)
) engine = MergeTree()
      order by (lift_no, alarm_type, record_date)
      partition by toYYYYMM(record_date);

create table running_state.running_state_detail as running_state.running_state_detail_sub
    engine = Distributed(ckcluster_2shards_2replicas, running_state, score_record_detail_sub,
             farmHash64(lift_no, record_date));


create table running_state.lift_running_state_sub
(
    id            String,
    lift_no       String,
    score_date    Date,
    last_score    Decimal32(2),
    reduce_score  Decimal32(2),
    recover_score Decimal32(2),
    current_score Decimal32(2)
)
    engine = MergeTree()
        order by (lift_no, score_date)
        partition by toYYYYMM(score_date);

create table running_state.lift_running_state as running_state.lift_running_state_sub
    engine = Distributed(ckcluster_2shards_2replicas, running_state, lift_running_state_sub,
             farmHash64(lift_no, score_date));
