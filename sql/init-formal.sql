-- on cluster ckcluster_2shards_2replicas

create table running_state.running_state_detail_sub on cluster ckcluster_2shards_2replicas
(
    record_date      Date,
    lift_no       String,
    alarm_type    String,
    alarm_num     Int32,
    duration_days Int8,
    scores        Decimal32(2),
    state_id String,
    tsid Decimal32(2)
) engine = MergeTree()
      order by (lift_no, alarm_type, record_date)
      partition by toYYYYMM(record_date);


create table running_state.lift_running_state_sub on cluster ckcluster_2shards_2replicas
(
    id String,
    lift_no String,
    score_date Date,
    last_score Decimal32(2),
    reduce_score Decimal32(2),
    recover_score Decimal32(2),
    current_score Decimal32(2)
)
    engine = MergeTree()
        order by (lift_no,score_date)
        PARTITION by toYYYYMM(score_date);

create table running_state.running_state_replica_sub on  cluster ckcluster_2shards_2replicas as running_state.lift_running_state
    engine=ReplicatedMergeTree('/clickhouse/tables/{shard}/running_state_replica_sub','{replica}')
partition by toYYYYMM(score_date)
order by (lift_no,score_date);


create table running_state.state_detail_replica_sub on  cluster ckcluster_2shards_2replicas as running_state.running_state_detail_sub
    engine=ReplicatedMergeTree('/clickhouse/tables/{shard}/state_detail_replica_sub','{replica}')
      order by (lift_no,record_date,alarm_type)
      partition by toYYYYMM(record_date);


create table running_state.running_state_detail on cluster ckcluster_2shards_2replicas as running_state.running_state_detail_sub
    engine= Distributed(ckcluster_2shards_2replicas,running_state,running_state_detail_sub,
            farmHash64(lift_no,record_date));

create table running_state.lift_running_state on cluster ckcluster_2shards_2replicas as running_state.lift_running_state_sub
    engine = Distributed(ckcluster_2shards_2replicas, running_state, lift_running_state_sub,
             farmHash64(lift_no, score_date));


create table running_state.algorithm_config
(
    id              UUID,
    dept_no         String,
    alarm_type      String,
    reduction       Int32,
    reduction_times Int32,
    reduction_rate  Float,
    recover_period  Int32
) engine = ReplacingMergeTree
      PRIMARY key id;

insert into running_state.algorithm_config(*)
values (generateUUIDv4(), '00', 1000126, 10, 1, 3, 10),
       (generateUUIDv4(), '00', 1000007, 20, 1, 3, 10),
       (generateUUIDv4(), '00', 1000154, 3, 1, 2, 3),
       (generateUUIDv4(), '00', 1000040, 3, 1, 3, 3),
       (generateUUIDv4(), '00', 9000006, 3, 1, 3, 3);


create table running_state.lift_info
(
    guid         UInt32,
    liftNo       String,
    registerCode String,
    liftName     String,
    nbhdGuid     Int8,
    streetCode   String,
    upTime       Datetime,
    ytStatus     String,
    lat          String,
    lon          String
) ENGINE = MySQL('192.168.4.168:8635', 'jczl_platform', 'jczl_lift', 'root', '5U=MydkO@Q9kkZM')
      SETTINGS
          connection_pool_size = 16,
          connection_max_tries = 3,
          connection_wait_timeout = 5,
          connection_auto_close = true
;

select * from lift_info limit 10;