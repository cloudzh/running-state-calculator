#!/usr/bin/expect
set DEPLOY_IMAGE                   [lindex $argv 0]
set DEPLOY_CONTAINER_NAME          [lindex $argv 1]
set CONSUL_IP                      [lindex $argv 2]
set CONSUL_PORT                    [lindex $argv 3]
set CI_JOB_TOKEN                   [lindex $argv 4]
set SERVICE_NAME                   [lindex $argv 5]
set SERVER_IP                      [lindex $argv 6]
set SERVER_PORT                    [lindex $argv 7]
set DEPLOY_JUMPER_PWD              [lindex $argv 8]
set SERVICE_PORT                   [lindex $argv 9]
set DEPLOY_ADDR                    [lindex $argv 10]
set PROFILE                        [lindex $argv 11]
set HEAP_SIZE                      [lindex $argv 12]
#超时时间设置长点，20分钟，完成部署任务会退出
set timeout 1200
set jumpserver 122.112.249.242
set port 22
set username zhangzhaoyun
spawn ssh -p $port $username@$jumpserver
expect {
    "*yes/no)?" { send "yes\r"; exp_continue  }
    "*assword:" { send "$DEPLOY_JUMPER_PWD\r"; exp_continue  }
    "Opt or ID*" { send "$SERVER_IP\r" }
}

expect {
    "ID>*" { send "1\r" }
    "web@" {send "\r"}
}

expect {
   "web@" {
        send "sudo docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.yun-ti.com\r"
        send "sudo docker pull $DEPLOY_IMAGE\r"
        send "sudo docker rm -f $SERVICE_NAME\r"
        send "sudo docker run -d --restart=always -e CONSUL_IP=$CONSUL_IP -e XMX=$HEAP_SIZE -e XMS=$HEAP_SIZE -e CONSUL_CONFIG_NAME=$SERVICE_NAME -e CONSUL_PORT=$CONSUL_PORT -e SERVER_IP=$SERVER_IP -e SERVER_PORT=$SERVICE_PORT -e PROFILE=$PROFILE -p $SERVER_PORT:$SERVICE_PORT -v /data/logs:/logs --name=$SERVICE_NAME $DEPLOY_IMAGE\r"
        send "exit\r"
    }
}

expect {
    "Opt or ID*" { send "q\r" }
}