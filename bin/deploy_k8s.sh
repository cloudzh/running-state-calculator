#!/bin/sh

export SERVICE_NAME=${1}
export SERVICE_PORT=${2}
export SERVICE_IMAGE=${3}
export SERVICE_REPLICAS=${4}
export XMS_SIZE=${5}
export XMX_SIZE=${6}
export CONSUL_IP=${7}
export PROFILE=${8}
export HEALTH_CHECK_URI=${9}

mkdir -p .generated
for f in templates/*.yml
do
    envsubst < $f > ".generated/$(basename $f)"
done

for f in .generated/*.yml
do
    cat $f
done

kubectl apply -f .generated/

#profile=test
#if [ "$PROFILE" == "$profile" ]; then
#    kubectl get pods -l=app=$SERVICE_NAME | awk 'NR>1 {printf " " $1} ' | xargs kubectl delete pods
#fi