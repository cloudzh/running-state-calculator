#!/usr/bin/expect
set DEPLOY_IMAGE                   [lindex $argv 0]
set DEPLOY_CONTAINER_NAME          [lindex $argv 1]
set CONSUL_IP                      [lindex $argv 2]
set CONSUL_PORT                    [lindex $argv 3]
set CI_JOB_TOKEN                   [lindex $argv 4]
set SERVICE_NAME                   [lindex $argv 5]
set SERVER_IP                      [lindex $argv 6]
set SERVER_PORT                    [lindex $argv 7]
set PROFILE                        [lindex $argv 8]
#超时时间设置长点，20分钟，完成部署任务会退出
set timeout 1200
set jumpserver 192.168.1.16
set port 22
set username root
spawn ssh -p $port $username@$jumpserver
expect {
    "*yes/no)?" { send "yes\r"; exp_continue  }
    "*assword:" { send "8b!Cm57&aXuT6xC\r" }
}
expect "*#" { send "docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.yun-ti.com\r" }

# expect "*#" { send "docker stop \\$(docker ps -f name=$SERVICE_NAME --format "table {{.ID}}") \r" }
expect "*#" { send "docker pull $DEPLOY_IMAGE\r" }
expect "*#" { send "docker rm -f  $SERVICE_NAME\r" }
expect "*#" { send "docker run --net=host -d --restart=always -e CONSUL_IP=$CONSUL_IP -e CONSUL_CONFIG_NAME=$SERVICE_NAME -e CONSUL_PORT=$CONSUL_PORT -e SERVER_IP=$SERVER_IP -e SERVER_PORT=$SERVER_PORT -e PROFILE=$PROFILE -v /data/logs:/logs --name=$SERVICE_NAME $DEPLOY_IMAGE\r" }
expect "*#" { send "exit\r" }
expect eof { exit }