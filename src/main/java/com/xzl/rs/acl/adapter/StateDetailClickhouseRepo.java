package com.xzl.rs.acl.adapter;

import com.xzl.rs.engine.model.RunningStateDetail;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;

@Repository
public class StateDetailClickhouseRepo {

    private static final String QUERY_FOR_LIST = """
            select * from running_state.running_state_detail where
            state_id = ? and
            lift_no = ?
            """;

    private static final String DELETE_BY_LIFTS_AND_DATE = """
            alter table running_state.running_state_detail on cluster ckcluster_2shards_2replicas delete where lift_no in (:liftNos) and  record_date > :date
            """;


    private static final String QUERY_FOR_OBJECT = """
            select * from running_state.running_state_detail where 
            state_id= ? and
            lift_no= ? and 
            alarm_type=? 
            """;

    private static final String QUERY_BY_LIFT_BETWEEN_DATE = """
            select * from running_state.running_state_detail where lift_no = ? 
            and record_date between ? and ? 
            order by record_date desc
            """;

    private static final String INSERT = """
            insert into running_state.running_state_detail (state_id,lift_no,alarm_type,alarm_num,scores,duration_days,record_date,tsid) values (?,?,?,?,?,?,?,?)
            """;

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate npJdbcTemplate;

    public StateDetailClickhouseRepo(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate npJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.npJdbcTemplate = npJdbcTemplate;
    }

    private RunningStateDetail convertFrom(ResultSet rs) throws SQLException {
        if (Objects.isNull(rs) || rs.getRow() == 0) return null;
        RunningStateDetail detail = new RunningStateDetail();
        detail.setDurationDays(rs.getInt("duration_days"));
        detail.setAlarmNum(rs.getInt("alarm_num"));
        detail.setScores(rs.getDouble("scores"));
        detail.setStateId(rs.getString("state_id"));
        detail.setLiftNo(rs.getString("lift_no"));
        detail.setAlarmType(rs.getString("alarm_type"));
        detail.setRecordDate(rs.getDate("record_date").toLocalDate());
        detail.setTsid(rs.getDouble("tsid"));

        return detail;
    }

    public RunningStateDetail query(String stateId, String liftNo, String alarmType) {

        return jdbcTemplate.query(QUERY_FOR_OBJECT, this::convertFrom, stateId, liftNo, alarmType);
    }

    public List<RunningStateDetail> queryForList(String stateId, String liftNo) {
        return jdbcTemplate.query(QUERY_FOR_LIST, (rs, rowNum) -> convertFrom(rs), stateId, liftNo);
    }

    public List<RunningStateDetail> queryByLiftNoAndBetweenDate(String liftNo, String beginDate, String endDate) {
        return jdbcTemplate.query(QUERY_BY_LIFT_BETWEEN_DATE, (rs, rowNum) -> convertFrom(rs), liftNo, beginDate, endDate);
    }

    public void deleteByLiftNosGreaterThanDate(List<String> liftNos, LocalDate date) {
        MapSqlParameterSource source = new MapSqlParameterSource("liftNos", liftNos);
        source.addValue("date", date);

        npJdbcTemplate.update(DELETE_BY_LIFTS_AND_DATE, source);
    }

    public void save(List<RunningStateDetail> details) {

        jdbcTemplate.batchUpdate(INSERT, details, 1000, (ps, detail) -> {
            ps.setString(1, detail.getStateId());
            ps.setString(2, detail.getLiftNo());
            ps.setString(3, detail.getAlarmType());
            ps.setInt(4, detail.getAlarmNum());
            ps.setDouble(5, detail.getScores());
            ps.setInt(6, detail.getDurationDays());
            ps.setString(7, detail.getRecordDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            ps.setDouble(8, detail.getTsid());
        });
    }

}
