package com.xzl.rs.acl.adapter;

import com.xzl.rs.engine.model.AlgorithmConfig;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.yandex.clickhouse.response.ClickHouseResultSet;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

@Repository
public class AlgorithmConfigClickHouseRepo {

    @Resource
    private JdbcTemplate jdbcTemplate;


    public AlgorithmConfig queryByDeptAndAlarmType(String deptNo, String alarmType) {
        return jdbcTemplate.query("select * from running_state.algorithm_config where dept_no=? and alarm_type=?", this::convertFrom, deptNo, alarmType);
    }

    private AlgorithmConfig convertFrom(ResultSet rs) throws SQLException {
        ClickHouseResultSet chrs = (ClickHouseResultSet) rs;
        if (Objects.isNull(chrs) || rs.getRow() == 0) return AlgorithmConfig.NULL_CONFIG;
        AlgorithmConfig config = new AlgorithmConfig();
        config.setId(chrs.getString("id"));
        config.setDeptNo(chrs.getString("dept_no"));
        config.setAlarmType(chrs.getString("alarm_type"));
        config.setReduction(chrs.getInt("reduction"));
        config.setReductionTimes(chrs.getInt("reduction_times"));
        config.setReductionRate(chrs.getDouble("reduction_rate"));
        config.setRecoverPeriod(chrs.getInt("recover_period"));
        return config;
    }

    public List<AlgorithmConfig> queryByDeptNo(String deptNo) {
        return jdbcTemplate.query("select * from running_state.algorithm_config where dept_no=?", (rs, rowNum) -> convertFrom(rs), deptNo);
    }


}
