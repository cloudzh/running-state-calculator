package com.xzl.rs.acl.adapter;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.xzl.rs.engine.model.LiftRunningState;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Slf4j
@Repository
public class RunningStateClickhouseRepo {
    private static final String QUERY_BY_LIFT_AND_SCORE_DATE = """
            select * from running_state.lift_running_state
            where lift_no= ? 
            and score_date = ?
            order by score_date desc 
            """;

    private static final String DELETE_BY_LIFTS_AND_DATE = """
            alter table running_state.lift_running_state_sub on cluster ckcluster_2shards_2replicas delete where lift_no in (:liftNos) and score_date > :date
            """;

    private static final String QUERY_BY_LIFT_AND_BETWEEN_DATE = """
            select * from running_state.lift_running_state where lift_no = ? and score_date between ? and ? order by score_date desc
            """;

    private static final String QUERY_BY_LIFTS_AND_BETWEEN_DATE = """
            select * from running_state.lift_running_state where lift_no in (:liftNos) and score_date between :beginDate and :endDate
            order by score_date desc
            """;

    private static final String QUERY_BY_LIFT_NOS_AND_DATE = """
            select * from running_state.lift_running_state where lift_no in (:liftNos) and score_date = :date
            order by score_date desc
            """;

    private static final String QUERY_BY_LIFT_AND_IN_DATE = """
            select * from running_state.lift_running_state where lift_no = :liftNo and score_date in (:dates)
            order by score_date desc
            """;


    private static final String INSERT_STATE_OF_DATE = """
            insert into running_state.lift_running_state 
            select generateUUIDv4(),lift_no,date_add(DAY,-1,score_date ),current_score ,0,0,current_score 
            from running_state.lift_running_state 
            where lift_no = ? and score_date = ?
            """;


    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate npJdbcTemplate;



    public RunningStateClickhouseRepo(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate npJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.npJdbcTemplate = npJdbcTemplate;
    }


    private LiftRunningState convertFrom(ResultSet rs) throws SQLException {
        if (Objects.isNull(rs) || rs.getRow() == 0) return null;
        LiftRunningState state = new LiftRunningState();
        state.setId(rs.getString("id"));
        state.setLastScore(rs.getDouble("last_score"));
        state.setCurrentScore(rs.getDouble("current_score"));
        state.setReduceScores(rs.getDouble("reduce_score"));
        state.setRecoverScores(rs.getDouble("recover_score"));
        state.setLiftNo(rs.getString("lift_no"));
        state.setScoreDate(rs.getDate("score_date").toLocalDate());

        return state;
    }

    public List<LiftRunningState> queryByLiftNosAndDate(List<String> liftNos, LocalDate stateDate) {
        if (Objects.isNull(liftNos) || Objects.isNull(stateDate) || liftNos.isEmpty()) return Collections.emptyList();
        MapSqlParameterSource params = new MapSqlParameterSource("liftNos", liftNos);
        params.addValue("date", stateDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        return npJdbcTemplate.query(QUERY_BY_LIFT_NOS_AND_DATE, params, (rs, rowNum) -> convertFrom(rs));
    }

    public List<LiftRunningState> queryByLiftNosBetweenDates(List<String> liftNos, String beginDate, String endDate) {
        if (Objects.isNull(liftNos) || Objects.isNull(beginDate) || Objects.isNull(endDate) || liftNos.isEmpty())
            return Collections.emptyList();

        MapSqlParameterSource params = new MapSqlParameterSource("liftNos", liftNos);
        params.addValue("beginDate", beginDate);
        params.addValue("endDate", endDate);

        return npJdbcTemplate.query(QUERY_BY_LIFTS_AND_BETWEEN_DATE, params, (rs, row) -> convertFrom(rs));
    }

    public List<LiftRunningState> queryByLiftNoAndBetweenDate(String liftNo, LocalDate beginDate, LocalDate endDate) {
        return jdbcTemplate.query(QUERY_BY_LIFT_AND_BETWEEN_DATE, (rs, rowNum) -> convertFrom(rs), liftNo, beginDate, endDate);
    }

    public LiftRunningState query(String liftNo, LocalDate scoreDate) {
        return jdbcTemplate.query(QUERY_BY_LIFT_AND_SCORE_DATE, rs -> {
            rs.next();
            return convertFrom(rs);
        }, liftNo, scoreDate);
    }

    public List<LiftRunningState> query(String liftNo, List<LocalDate> dates) {
        if (Strings.isNullOrEmpty(liftNo) || Objects.isNull(dates) || dates.isEmpty()) return Collections.emptyList();

        List<String> dateList = dates.stream().map(date -> date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))).collect(Collectors.toList());


        MapSqlParameterSource param = new MapSqlParameterSource("liftNo", liftNo);
        param.addValue("dates", dateList);

        return npJdbcTemplate.query(QUERY_BY_LIFT_AND_IN_DATE, param, ((rs, rowNum) -> convertFrom(rs)));
    }

    private static final String INSERT = """
            insert into running_state.lift_running_state(id,lift_no,score_date,last_score,reduce_score,recover_score,current_score) 
            values(?,?,?,?,?,?,?)
            """;

    public void save(LiftRunningState state) {
        jdbcTemplate.update(INSERT, ps -> {
            prepare(state, ps);
        });
    }

    private void prepare(LiftRunningState state, PreparedStatement ps) throws SQLException {
        ps.setString(1, state.getId());
        ps.setString(2, state.getLiftNo());
        ps.setString(3, state.getScoreDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        ps.setDouble(4, state.getLastScore());
        ps.setDouble(5, state.getReduceScores());
        ps.setDouble(6, state.getRecoverScores());
        ps.setDouble(7, state.getCurrentScore());
    }

    private final int BATCH_SIZE = 2000;

    public void saveNow(List<LiftRunningState> states) {
        jdbcTemplate.batchUpdate(INSERT, states, BATCH_SIZE, (ps, state) -> prepare(state, ps));
    }

    public void insertStateBasedOnDate(String liftNo, LocalDate date) {
        jdbcTemplate.update(INSERT_STATE_OF_DATE, pss -> {
            pss.setString(1, liftNo);
            pss.setString(2, date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));

            pss.execute();
        });
    }

    public void deleteByLiftNosGreaterThanDate(List<String> liftNos, LocalDate date) {
        MapSqlParameterSource source = new MapSqlParameterSource("liftNos", liftNos);
        source.addValue("date", date);

        npJdbcTemplate.update(DELETE_BY_LIFTS_AND_DATE, source);
    }


}

