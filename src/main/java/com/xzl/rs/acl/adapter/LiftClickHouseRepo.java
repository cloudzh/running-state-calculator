package com.xzl.rs.acl.adapter;

import com.google.common.collect.Maps;
import com.xzl.rs.acl.pl.LiftInfo;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class LiftClickHouseRepo {

    private static final String QUERY_LIFT = """
            select * from running_state.lift_info where ytStatus !='10'
            and toDate(upTime) <= ? order by guid limit ?,?
            """;

    private static final String QUERY_LIFT_BY_LIFT_NOS = """
            select * from running_state.lift_info where ytStatus != '10'
            and liftNo in (:liftNos)
            """;

    private static final String COUNT_LIFT = """
            select count(guid) idCount,max(guid) maxId from running_state.lift_info where ytStatus != '10' and toDate(upTime) <= ?
            """;

    private static final String QUERY_BY_RANGE_ID = """
            select * from running_state.lift_info where ytStatus !='10' and guid between  ? and ? and toDate(upTime) <=?
            """;

    final private JdbcTemplate jdbcTemplate;
    final private NamedParameterJdbcTemplate npJdbcTemplate;

    public LiftClickHouseRepo(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate npJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.npJdbcTemplate = npJdbcTemplate;
    }

    public List<LiftInfo> queryRangeId(Integer beginId, Integer endId, LocalDate upTime) {
        return jdbcTemplate.query(QUERY_BY_RANGE_ID,
                pss -> {
                    pss.setInt(1, beginId);
                    pss.setInt(2, endId);
                    pss.setString(3, upTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                },
                (rs, rowNum) -> convert(rs));
    }

    private static Map<String, Integer> extractData(ResultSet rs) {
        try {
            rs.next();
            Map<String, Integer> result = Maps.newHashMap();
            result.put("idCount", rs.getInt(1));
            result.put("maxId", rs.getInt(2));
            return result;
        } catch (SQLException e) {
            return new HashMap<>() {{
                put("idCount", 0);
                put("maxId", 0);
            }};
        }
    }

    private LiftInfo convert(ResultSet rs) {
        try {
            LiftInfo info = new LiftInfo();
            info.setLiftNo(rs.getString("liftNo"));
            info.setOnlineTime(rs.getDate("upTime").toLocalDate());
            info.setStreetCode(rs.getString("streetCode"));
            info.setYtStatus(rs.getString("ytStatus"));
            return info;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public List<LiftInfo> queryByLiftNos(List<String> liftNos) {
        MapSqlParameterSource param = new MapSqlParameterSource("liftNos", liftNos);

        return npJdbcTemplate.query(QUERY_LIFT_BY_LIFT_NOS, param, (rs, rowNum) -> convert(rs));
    }

    public Map<String, Integer> count(LocalDate onlineDate) {
        return jdbcTemplate.query(COUNT_LIFT, LiftClickHouseRepo::extractData, onlineDate);
    }

    public List<LiftInfo> queryLifts(LocalDate onlineDate, Integer offset, Integer limit) {

        return jdbcTemplate.query(QUERY_LIFT,
                pss -> {
                    pss.setString(1, onlineDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                    pss.setInt(2, offset);
                    pss.setInt(3, limit);
                },
                (rs, rowNum) -> convert(rs));
    }

}
