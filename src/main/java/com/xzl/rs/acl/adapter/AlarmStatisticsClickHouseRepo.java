package com.xzl.rs.acl.adapter;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.xzl.rs.ohs.pl.AlarmTypeStatistics;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Repository
public class AlarmStatisticsClickHouseRepo {

    final JdbcTemplate jdbcTemplate;

    final NamedParameterJdbcTemplate npJdbcTemplate;

    private static final String sql = """
            select * from alarm_statistics.cl_alarm_statistics where 
            liftNo = :liftNo and alarmType in (:types) and statisticsDate = :date
            """;

    private static final String QUERY_BETWEEN_DATES = """
            select * from alarm_statistics.cl_alarm_statistics 
            where liftNo=:liftNo and 
            alarmType in(:types) and
            statisticsDate between :beginDate and :endDate
            order by statisticsDate
            """;

    public AlarmStatisticsClickHouseRepo(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate npJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.npJdbcTemplate = npJdbcTemplate;
    }

    private AlarmTypeStatistics convertFrom(ResultSet rs, int rowNum) {
        AlarmTypeStatistics as = new AlarmTypeStatistics();
        try {
            as.setAlarmType(rs.getString("alarmType"));
            as.setLiftNo(rs.getString("liftNo"));
            as.setAlarmTypeCount(rs.getInt("alarmCount"));
            as.setStatisticsDate(rs.getDate("statisticsDate").toLocalDate());
            return as;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public List<AlarmTypeStatistics> queryByParam(String liftNo, String alarmTypes, LocalDate date) {
        if (Strings.isNullOrEmpty(alarmTypes)) return Collections.emptyList();
        List<String> types = Splitter.on(",").splitToList(alarmTypes);

        MapSqlParameterSource param = new MapSqlParameterSource("types", types);
        param.addValue("liftNo", liftNo);
        param.addValue("date", date);

        return npJdbcTemplate.query(sql, param, this::convertFrom);
    }

    public List<AlarmTypeStatistics> queryByParamBetweenDates(String liftNo, String alarmTypes, LocalDate beginDate, LocalDate endDate) {
        if (Strings.isNullOrEmpty(alarmTypes)) return Collections.emptyList();
        List<String> typeList = Splitter.on(",").splitToList(alarmTypes);
        MapSqlParameterSource param = new MapSqlParameterSource("types", typeList);
        param.addValue("liftNo", liftNo);
        param.addValue("beginDate", beginDate);
        param.addValue("endDate", endDate);

        return npJdbcTemplate.query(QUERY_BETWEEN_DATES, param, this::convertFrom);
    }
}
