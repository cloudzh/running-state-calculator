package com.xzl.rs.acl.pl;

import lombok.Data;

import java.time.LocalDate;

@Data
public class LiftInfo {

    private String liftNo;
    private String streetCode;
    private String maintNo;
    private String govNo;
    private String ytStatus;
    private LocalDate onlineTime;
}
