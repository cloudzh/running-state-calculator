package com.xzl.rs.acl.repo;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.xzl.rs.acl.adapter.AlgorithmConfigClickHouseRepo;
import com.xzl.rs.engine.model.AlgorithmConfig;
import com.xzl.rs.engine.repository.AlgorithmConfigRepository;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.Duration;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class AlgorithmConfigRepoImpl implements AlgorithmConfigRepository {

    private LoadingCache<String, AlgorithmConfig> alarmTypeConfigCache = Caffeine.newBuilder()
            .expireAfterWrite(Duration.ofHours(12))
            .build(key -> load(key));

    private LoadingCache<String, AlgorithmConfig> configCache = Caffeine.newBuilder()
            .expireAfterWrite(Duration.ofHours(12)).build(this::loadConfig);

    @Resource
    private AlgorithmConfigClickHouseRepo clickHouseRepo;

    @Override
    public AlgorithmConfig findConfig(String deptNo, String alarmType) {
        return alarmTypeConfigCache.get(String.format("%s#%s", deptNo, alarmType));
    }


    private AlgorithmConfig load(String key) {
        if (Objects.isNull(key) || key.isEmpty()) return null;
        String[] split = key.split("#");
        return clickHouseRepo.queryByDeptAndAlarmType(split[0], split[1]);
    }

    @Override
    public void updateConfig(AlgorithmConfig config) {

    }

    @Override
    public String findAllAlarmTypes(String deptNo) {
        return configCache.get(deptNo).getConfigAlarmTypes();
    }

    @Override
    public AlgorithmConfig findAllConfig(String deptNo) {
        return configCache.get(deptNo);
    }

    private AlgorithmConfig loadConfig(String deptNo) {
        List<AlgorithmConfig> configs = clickHouseRepo.queryByDeptNo(deptNo);
        return new AlgorithmConfig(configs);
    }

    @Override
    public void save(AlgorithmConfig config) {

    }

    @Override
    public void refreshCache(String deptNo) {
        configCache.refresh(deptNo);


    }

    private String findAllAlarmTypesFromRepo(String deptNo) {
        List<AlgorithmConfig> configs = clickHouseRepo.queryByDeptNo(deptNo);
        if (null == configs || configs.isEmpty()) return "";

        return configs.stream().map(AlgorithmConfig::getAlarmType).collect(Collectors.joining(","));

    }
}
