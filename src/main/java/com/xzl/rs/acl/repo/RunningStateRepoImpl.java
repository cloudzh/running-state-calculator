package com.xzl.rs.acl.repo;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.xzl.rs.acl.adapter.RunningStateClickhouseRepo;
import com.xzl.rs.acl.adapter.StateDetailClickhouseRepo;
import com.xzl.rs.engine.model.LiftRunningState;
import com.xzl.rs.engine.model.RunningStateDetail;
import com.xzl.rs.engine.repository.LiftRunningStateRepository;
import com.xzl.rs.engine.repository.RunningStateDetailRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

import java.time.Duration;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Slf4j
@Repository
public class RunningStateRepoImpl implements LiftRunningStateRepository, RunningStateDetailRepository {

    final private RunningStateClickhouseRepo runningStateClickhouseRepo;

    final private StateDetailClickhouseRepo stateDetailClickhouseRepo;

    private LoadingCache<CacheKey, LiftRunningState> stateCache = Caffeine.newBuilder()
            .expireAfterWrite(Duration.ofDays(3))
            .maximumSize(300000)
            .build(key -> findOrCreate(key.liftNo, key.scoreDate));

    private BlockingQueue<LiftRunningState> blockingQueue = new ArrayBlockingQueue<>(200000);

    private final int BATCH_SIZE = 2000;


    public RunningStateRepoImpl(RunningStateClickhouseRepo runningStateClickhouseRepo, StateDetailClickhouseRepo stateDetailClickhouseRepo) {
        this.runningStateClickhouseRepo = runningStateClickhouseRepo;
        this.stateDetailClickhouseRepo = stateDetailClickhouseRepo;
    }

    @Override
    public void refreshCache(LiftRunningState state) {
        stateCache.put(new CacheKey(state.getLiftNo(), state.getScoreDate()), state);
        stateCache.invalidate(new CacheKey(state.getLiftNo(), state.getScoreDate().minusDays(1)));
    }

    @Override
    public LiftRunningState getFromCache(LiftRunningState state) {
        log.warn("State Cache Stats : {}", stateCache.stats());
        return stateCache.get(new CacheKey(state.getLiftNo(), state.getScoreDate()));
    }

    @Override
    public void deleteState(List<String> liftNos, LocalDate beginDate) {
        stateDetailClickhouseRepo.deleteByLiftNosGreaterThanDate(liftNos, beginDate);
        runningStateClickhouseRepo.deleteByLiftNosGreaterThanDate(liftNos, beginDate);
    }

    @Override
    public void saveBatch(List<LiftRunningState> states) {
        states.stream().forEach(s -> {
            try {
                blockingQueue.offer(s, 5, TimeUnit.MINUTES);
            } catch (InterruptedException e) {
                saveNow(Lists.newArrayList(s));
            }
        });
    }

    private void saveNow(List<LiftRunningState> states) {
        runningStateClickhouseRepo.saveNow(states);

        List<RunningStateDetail> details = states.stream()
                .map(LiftRunningState::getDetails).filter(param -> !(Objects.isNull(param) || param.isEmpty()))
                .flatMap(List::stream).collect(Collectors.toList());

        if (!Objects.isNull(details) && !details.isEmpty()) stateDetailClickhouseRepo.save(details);

    }


    @Override
    public void save(LiftRunningState state) {
        if (log.isDebugEnabled()) log.debug("state : {}", state);
        refreshCache(state);

        try {
            blockingQueue.offer(state, 5, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            saveNow(Lists.newArrayList(state));
        }
//        runningStateClickhouseRepo.save(state);
//
//        List<RunningStateDetail> details = state.getDetails();
//        if (Objects.isNull(details) || details.isEmpty()) return;
//        stateDetailClickhouseRepo.save(details);
    }

    @Override
    public List<LiftRunningState> queryMonthTrends(String liftNo, int year) {
        LocalDate now = LocalDate.now();

        if (now.getYear() < year) return Collections.emptyList();

        if (year == now.getYear()) {

        } else {

        }

        return runningStateClickhouseRepo.query(liftNo, Collections.emptyList());
    }

    @Override
    public List<LiftRunningState> queryByLiftNosBetweenDates(List<String> liftNos, String beginDate, String endDate) {

        return runningStateClickhouseRepo.queryByLiftNosBetweenDates(liftNos, beginDate, endDate);
    }

    @Override
    public List<LiftRunningState> queryBetweenMonths(String liftNo, String beginMonth, String endMonth) {
        if (Strings.isNullOrEmpty(liftNo) || Strings.isNullOrEmpty(beginMonth) || Strings.isNullOrEmpty(endMonth))
            return Collections.emptyList();

        YearMonth begin = YearMonth.parse(beginMonth, DateTimeFormatter.ofPattern("yyyy-MM"));
        YearMonth end = YearMonth.parse(endMonth, DateTimeFormatter.ofPattern("yyyy-MM"));

        List<LocalDate> dates = Lists.newArrayList();


        while (begin.isBefore(end)) {
            LocalDate date = LocalDate.of(begin.getYear(), begin.getMonthValue(), begin.lengthOfMonth());
            begin = begin.plusMonths(1);

            dates.add(date);

        }
        return runningStateClickhouseRepo.query(liftNo, dates);
    }

    @Override
    public List<LiftRunningState> queryDateTrends(String liftNo, String beginDate, String endDate) {
        return runningStateClickhouseRepo.queryByLiftNoAndBetweenDate(liftNo,
                LocalDate.parse(beginDate, DateTimeFormatter.ofPattern("yyyy-MM-dd")),
                LocalDate.parse(endDate, DateTimeFormatter.ofPattern("yyyy-MM-dd")));
    }

    @Override
    public List<RunningStateDetail> queryDetails(String liftNo, String beginDate, String endDate) {
        return stateDetailClickhouseRepo.queryByLiftNoAndBetweenDate(liftNo, beginDate, endDate);
    }

    @Override
    public LiftRunningState findLatestStateOfScoreDate(String liftNo, LocalDate scoreDate) {

        return stateCache.get(new CacheKey(liftNo, scoreDate));
    }

    private LiftRunningState findOrCreate(String liftNo, LocalDate scoreDate) {

        LiftRunningState state = runningStateClickhouseRepo.query(liftNo, scoreDate);
        if (Objects.isNull(state) || state.getId().isEmpty()) {

            state = LiftRunningState.createDefault(liftNo, scoreDate);

            runningStateClickhouseRepo.save(state);

            return state;
        }

        List<RunningStateDetail> detail = stateDetailClickhouseRepo.queryForList(state.getId(), liftNo);

        return state.initHistory(detail);
    }

    @Override
    public List<LiftRunningState> queryByLiftNosAndDate(List<String> liftNos, LocalDate date) {
        return runningStateClickhouseRepo.queryByLiftNosAndDate(liftNos, date);
    }

    @Override
    public void insertStateOfYesterday(String liftNo, LocalDate date) {
        runningStateClickhouseRepo.insertStateBasedOnDate(liftNo, date);
    }

    @Scheduled(fixedRate = 10, timeUnit = TimeUnit.SECONDS)
    @Override
    public void doBatchInsertToClickHouse() {
        doNow();
    }

    private void doNow() {
        if (blockingQueue.size() == 0) return;
        long timestamp = System.currentTimeMillis();
        List<LiftRunningState> states = null;
        try {
            states = Lists.newArrayListWithCapacity(blockingQueue.size());
            blockingQueue.drainTo(states);
            saveNow(states);
            log.warn("{} Blocking State Size:{}, Queue Size : {},Cast: {}", LocalDate.now(), states.size(), blockingQueue.size(), System.currentTimeMillis() - timestamp);
        } catch (Exception e) {
            log.error("doBatchInertToClickHouse Error", e);
            if (!Objects.isNull(states)) saveBatch(states);
        }
    }

    @Scheduled(cron = "0/10 * * * * ?")
    @Override
    public void doBatchInsertToClickHouseJob2() {

        doNow();
    }

    private static class CacheKey {
        String liftNo;
        LocalDate scoreDate;

        public CacheKey(String liftNo, LocalDate scoreDate) {
            this.liftNo = liftNo;
            this.scoreDate = scoreDate;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            CacheKey cacheKey = (CacheKey) o;
            return com.google.common.base.Objects.equal(liftNo, cacheKey.liftNo) && com.google.common.base.Objects.equal(scoreDate, cacheKey.scoreDate);
        }

        @Override
        public int hashCode() {
            return com.google.common.base.Objects.hashCode(liftNo, scoreDate);
        }
    }
}
