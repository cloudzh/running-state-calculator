package com.xzl.rs.acl.provider;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.xzl.rs.acl.adapter.AlarmStatisticsClickHouseRepo;
import com.xzl.rs.ohs.pl.AlarmTypeStatistics;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Component
public class AlarmStatisticsProvider {

    final AlarmStatisticsClickHouseRepo statisticsRepo;

    public AlarmStatisticsProvider(AlarmStatisticsClickHouseRepo statisticsRepo) {
        this.statisticsRepo = statisticsRepo;
    }

    public List<AlarmTypeStatistics> fetchStatistics(String liftNo, String alarmTypes, LocalDate date) {
        return statisticsRepo.queryByParam(liftNo, alarmTypes, date);
    }

    public Table<LocalDate, String, AlarmTypeStatistics> fetchStatisticsBetweenDates(String liftNo, String alarmTypes, LocalDate startDate, LocalDate endDate) {
        List<AlarmTypeStatistics> statistics = statisticsRepo.queryByParamBetweenDates(liftNo, alarmTypes, startDate, endDate);

        Table<LocalDate, String, AlarmTypeStatistics> table = HashBasedTable.create();
        if (Objects.isNull(statistics) || statistics.isEmpty()) return table;

        statistics.forEach(ats -> table.put(ats.getStatisticsDate(), ats.getAlarmType(), ats));
        return table;
    }
}
