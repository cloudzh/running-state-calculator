package com.xzl.rs.acl.provider;

import com.xzl.rs.acl.adapter.LiftClickHouseRepo;
import com.xzl.rs.acl.pl.LiftInfo;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@Repository
public class LiftProvider {
    private final LiftClickHouseRepo liftRepo;

    public LiftProvider(LiftClickHouseRepo liftRepo) {
        this.liftRepo = liftRepo;
    }

    public List<LiftInfo> fetchLiftsNeedCalculate(LocalDate onlineDate, Integer offset, Integer limit) {
        return liftRepo.queryLifts(onlineDate, offset, limit);
    }

    public List<LiftInfo> fetchLiftsNeedCalculate(Integer beginId, Integer endId, LocalDate onlineDate) {
        return liftRepo.queryRangeId(beginId, endId, onlineDate);
    }

    public List<LiftInfo> fetchListByLiftNo(List<String> liftNos) {
        return liftRepo.queryByLiftNos(liftNos);
    }

    public Map<String, Integer> count(LocalDate online) {
        return liftRepo.count(online);
    }
}
