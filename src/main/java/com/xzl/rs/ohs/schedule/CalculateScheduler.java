package com.xzl.rs.ohs.schedule;

import com.xzl.rs.engine.service.CalculateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class CalculateScheduler {

    @Resource
    private CalculateService calculateService;

    @Scheduled(cron = "0 0 1 * * ?", zone = "Asia/Shanghai")
    public void doCalculate() {
        log.info("do calculate-----------------------------");
        calculateService.calculate();
    }



}
