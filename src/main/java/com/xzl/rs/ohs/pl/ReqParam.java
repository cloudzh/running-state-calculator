package com.xzl.rs.ohs.pl;

import lombok.Data;

import java.util.List;

@Data
public class ReqParam {

    private String beginDate;
    private String endDate;
    private List<String> liftNos;

    private Integer limit;
    private Integer offset;
}
