package com.xzl.rs.ohs.pl;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.common.base.Splitter;
import com.google.common.collect.Maps;
import lombok.Data;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Data
public class AlarmTypeStatistics {

    private String liftNo;
    private String alarmType;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate statisticsDate;
    @JsonAlias("count")
    private Integer alarmTypeCount;


    public static AlarmTypeStatistics buildDefault(String alarmType) {
        AlarmTypeStatistics ats = new AlarmTypeStatistics();
        ats.setAlarmTypeCount(0);
        ats.setAlarmType(alarmType);
        return ats;
    }

    public static Collection<AlarmTypeStatistics> buildWithAlarmTypes(String alarmTypes, Collection<AlarmTypeStatistics> statistics) {
        List<String> types = Splitter.on(",").splitToList(alarmTypes);

        Map<String, AlarmTypeStatistics> sourceMap = Maps.newHashMap();
        Map<String, AlarmTypeStatistics> resultMap = Maps.newHashMap();

        if (!Objects.isNull(statistics)) statistics.forEach(ats -> sourceMap.put(ats.getAlarmType(), ats));

        types.forEach(type -> {
            if (sourceMap.containsKey(type)) {
                resultMap.put(type, sourceMap.get(type));
            } else {
                resultMap.put(type, AlarmTypeStatistics.buildDefault(type));
            }
        });
        return resultMap.values();
    }


}
