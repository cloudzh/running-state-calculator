package com.xzl.rs.ohs.pl;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Objects;

@Data
@Slf4j
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class RespResult<T> implements Serializable {
    private static final long serialVersionUID = 1L;


    /***默认成功***/
    private Integer code = 0;
    private String message;
    private T data;

    /**
     * 成功消息, 无数据体
     */
    public static RespResult success() {
        return success("success");
    }


    /**
     * 成功消息
     */
    public static RespResult success(String data) {
        return new RespResult<>(0, "success", data);
    }

    /**
     * 成功消息
     */
    public static RespResult success(Object data) {
        return new RespResult<>(0, "success", data);
    }

    /**
     * 成功消息
     */
    public static RespResult success(String message, Object data) {
        return new RespResult<>(0, message, data);
    }


    public static RespResult fail() {
        return new RespResult<>(99999999, "fail", null);
    }


    public boolean check() {
        boolean b = code == 0 && Objects.nonNull(data);
        if (b) {
            return true;
        }
        throw new RuntimeException(code.toString());
    }

    public boolean checkCode() {
        boolean b = code == 0;
        if (b) {
            return true;
        }
        throw new RuntimeException(code.toString());
    }

}
