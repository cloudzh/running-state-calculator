package com.xzl.rs.ohs.api;


import com.google.common.base.Strings;
import com.xzl.rs.engine.model.LiftRunningState;
import com.xzl.rs.engine.service.CalculateService;
import com.xzl.rs.ohs.pl.ReqParam;
import com.xzl.rs.ohs.pl.RespResult;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;

@RestController
@RequestMapping("/state")
public class CalculateController {

    private final CalculateService calculateService;

    public CalculateController(CalculateService calculateService) {
        this.calculateService = calculateService;
    }

    private static final String TOKEN = "23222fa133c4506aaad380fd31aa486a";
    //==================================================================================//

    /**
     * @param levelScope
     * @return
     */
    @PutMapping("level-scope")
    public RespResult resetLevelScope(@RequestParam("levelScope") String levelScope, @RequestHeader("token") String token) {
        if (Strings.isNullOrEmpty(token) || !TOKEN.equals(token)) return RespResult.fail();

        LiftRunningState.setLevelScope(levelScope);
        return RespResult.success();
    }

    @PostMapping("/config/{deptNo}")
    public RespResult refreshAlgorithmConfig(@PathVariable("deptNo") String deptNo, @RequestHeader("token") String token) {
        if (Strings.isNullOrEmpty(token) || !TOKEN.equals(token)) return RespResult.fail();
        return RespResult.success(calculateService.refreshConfig(deptNo));
    }

    @PostMapping("re-calc/{type}")
    public RespResult recompute(@RequestBody ReqParam req, @RequestHeader("token") String token, @PathVariable("type") Integer type) {
        if (Strings.isNullOrEmpty(token) || !TOKEN.equals(token)) return RespResult.fail();
        calculateService.recompute(req.getLiftNos(), LocalDate.parse(req.getBeginDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd")), type);
        return RespResult.success();
    }

    @PostMapping("calc-offset")
    public RespResult computeByOffset(@RequestBody ReqParam req, @RequestHeader("token") String token) {
        if (Strings.isNullOrEmpty(token) || !TOKEN.equals(token)) return RespResult.fail();
        calculateService.computeWithOffset(req.getOffset(), req.getLimit(),
                LocalDate.parse(req.getBeginDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd")),
                LocalDate.parse(req.getEndDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        return RespResult.success();
    }


    @PostMapping("calc/{type}")
    public RespResult compute(@RequestBody ReqParam req, @PathVariable("type") Integer type, @RequestHeader("token") String token) {

        if (Strings.isNullOrEmpty(token) || !TOKEN.equals(token)) return RespResult.fail();

        calculateService.compute(
                req.getLiftNos(),
                LocalDate.parse(req.getBeginDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd")),
                LocalDate.parse(req.getEndDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd")),
                type
        );
        return RespResult.success();
    }

    @PutMapping("manual/{status}")
    public RespResult resetManualModel(@PathVariable("status") int status, @RequestHeader("token") String token) {
        if (Strings.isNullOrEmpty(token) || !TOKEN.equals(token)) return RespResult.fail();

        return RespResult.success(calculateService.set(status == 1 ? true : false));
    }

    @GetMapping("executor-state")
    public RespResult<Map> executorState() {
        return RespResult.success(calculateService.taskPoolStatus());
    }

    @GetMapping("/{liftNo}/{scoreDate}/cache")
    public RespResult<LiftRunningState> getFromCache(@PathVariable("liftNo") String liftNo, @PathVariable("scoreDate") String LocalDate) {
        return RespResult.success(calculateService.getFromCache(liftNo, LocalDate));
    }
}
