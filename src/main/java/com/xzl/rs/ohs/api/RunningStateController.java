package com.xzl.rs.ohs.api;

import com.xzl.rs.engine.model.LiftRunningState;
import com.xzl.rs.engine.service.CalculateService;
import com.xzl.rs.engine.service.LiftRunningStateService;
import com.xzl.rs.ohs.pl.ReqParam;
import com.xzl.rs.ohs.pl.RespResult;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/state")
public class RunningStateController {

    private final LiftRunningStateService stateService;


    public RunningStateController(LiftRunningStateService stateService) {
        this.stateService = stateService;
    }

    /**
     * 获取电梯某年的月度趋势 (*)
     *
     * @param liftNo
     * @param year
     * @return
     */
//    @GetMapping("{liftNo}/{year}/month-trends")
    public RespResult<List<LiftRunningState>> queryMonthTrendsByLift(@PathVariable("liftNo") String liftNo, @PathVariable("year") int year) {
        throw new RuntimeException("Not Support Now");
//        return RespResult.success(stateService.query(liftNo, year));
    }

    @GetMapping("{liftNo}/month-trends")
    public RespResult<List<LiftRunningState>> queryMonthTrendsBetweenMonths(@PathVariable("liftNo") String liftNo, @RequestParam("beginMonth") String beginMonth, @RequestParam("endMonth") String endMonth) {

        List<LiftRunningState> data = stateService.queryBetweenMonths(liftNo, beginMonth, endMonth);
        data.forEach(state -> {
            if (0 > state.getCurrentScore()) state.setCurrentScore(0.0);
        });
        return RespResult.success(data);
    }

    /**
     * 获取电梯某个时间范围内的运行分数；
     *
     * @param liftNo    电梯编号
     * @param beginDate 开始日期 （yyyy-MM-dd）
     * @param endDate   结束日期 (yyyy-MM-dd)
     * @return
     */
    @GetMapping("{liftNo}/date-trends")
    public RespResult<List<LiftRunningState>> queryDateTreadByLift(@PathVariable("liftNo") String liftNo, @RequestParam("beginDate") String beginDate, @RequestParam("endDate") String endDate) {
        List<LiftRunningState> data = stateService.queryBetweenDates(liftNo, beginDate, endDate);
        data.forEach(state -> {
            if (0 > state.getCurrentScore()) state.setCurrentScore(0.0);
        });
        return RespResult.success(data);
    }

    /**
     * 获取电梯最近的运行分数
     *
     * @param liftNos
     * @return
     */
    @PostMapping("/latest")
    public RespResult<List<LiftRunningState>> queryLatest(@RequestBody List<String> liftNos) {

        List<LiftRunningState> data = stateService.queryLatest(liftNos);
        data.forEach(state -> {
            if (0 > state.getCurrentScore()) state.setCurrentScore(0.0);
        });

        return RespResult.success(data);
    }

    /**
     * 获取电梯时间范围内的运行数据
     *
     * @param param
     * @return
     */
    @PostMapping("/date-trends")
    public RespResult<List<LiftRunningState>> query(@RequestBody ReqParam param) {
        List<LiftRunningState> data = stateService.queryByLiftNosBetweenDate(param.getLiftNos(), param.getBeginDate(), param.getEndDate());
        data.forEach(state -> {
            if (0 > state.getCurrentScore()) state.setCurrentScore(0.0);
        });
        return RespResult.success(data);
    }



}
