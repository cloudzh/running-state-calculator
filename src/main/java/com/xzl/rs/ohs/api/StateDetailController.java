package com.xzl.rs.ohs.api;

import com.google.common.collect.Lists;
import com.xzl.rs.engine.model.RunningStateDetail;
import com.xzl.rs.engine.service.LiftRunningStateService;
import com.xzl.rs.ohs.pl.RespResult;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.random.RandomGenerator;

@RestController
@RequestMapping("/state")
public class StateDetailController {

    private final LiftRunningStateService stateService;

    public StateDetailController(LiftRunningStateService stateService) {
        this.stateService = stateService;
    }

    /**
     * 获取电梯某个时间范围内的运行状态明细 (*)
     *
     * @param liftNo    电梯编号
     * @param beginDate 开始日期 （yyyy-MM-dd）
     * @param endDate   结束日期 （yyyy-MM-dd）
     * @return
     */
    @GetMapping("{liftNo}/details")
    public RespResult queryRecordDetailByLift(@PathVariable("liftNo") String liftNo,
                                              @RequestParam("beginDate") String beginDate,
                                              @RequestParam("endDate") String endDate) {


//        LocalDate begin = LocalDate.parse(beginDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
//        LocalDate end = LocalDate.parse(endDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
//
//        List<RunningStateDetail> detailList = Lists.newArrayList();
//
//        while (begin.isBefore(end)) {
//            RunningStateDetail detail = new RunningStateDetail();
//            detail.setLiftNo(liftNo);
//            detail.setRecordDate(begin);
//            RandomGenerator d = RandomGenerator.getDefault();
//            detail.setAlarmNum(d.nextInt(10));
//            detail.setAlarmType("1000007");
//            detail.setScores(scale(d.nextDouble(-50,100)));
//            detail.setDurationDays(d.nextInt(5));
//            detailList.add(detail);
//
//            begin = begin.plusDays(1);
//        }
//
//        return RespResult.success(detailList);

        return RespResult.success(stateService.queryDetails(liftNo, beginDate, endDate));
    }

    private Double scale(Double d) {
        BigDecimal bd = new BigDecimal(d);

        return bd.setScale(1, RoundingMode.HALF_UP).doubleValue();
    }
}
