package com.xzl.rs.configuration;

import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ru.yandex.clickhouse.ClickHouseDataSource;
import ru.yandex.clickhouse.settings.ClickHouseProperties;

import javax.sql.DataSource;

@Configuration
public class ClickhouseConfiguration {


    @Bean
    public DataSource dataSource(DataSourceProperties properties) {
        ClickHouseProperties chp = new ClickHouseProperties();
        chp.setUser(properties.getUsername());
        chp.setPassword(properties.getPassword());

        ClickHouseDataSource dataSource = new ClickHouseDataSource(properties.determineUrl(), chp);

        return dataSource;
    }

    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean
    public NamedParameterJdbcTemplate npJdbcTemplate(DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }
}
