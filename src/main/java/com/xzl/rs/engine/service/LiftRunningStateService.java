package com.xzl.rs.engine.service;

import com.google.common.collect.Lists;
import com.google.common.collect.Table;
import com.xzl.rs.acl.pl.LiftInfo;
import com.xzl.rs.acl.provider.AlarmStatisticsProvider;
import com.xzl.rs.engine.model.AlgorithmConfig;
import com.xzl.rs.engine.model.LiftRunningState;
import com.xzl.rs.engine.model.RunningStateDetail;
import com.xzl.rs.engine.repository.AlgorithmConfigRepository;
import com.xzl.rs.engine.repository.LiftRunningStateRepository;
import com.xzl.rs.engine.repository.RunningStateDetailRepository;
import com.xzl.rs.ohs.pl.AlarmTypeStatistics;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Slf4j
@Service
public class LiftRunningStateService {

    final private LiftRunningStateRepository stateRepository;

    final private RunningStateDetailRepository detailRepository;

    final private AlgorithmConfigRepository configRepository;

    final private AlarmStatisticsProvider statisticsProvider;


    public LiftRunningStateService(LiftRunningStateRepository stateRepository, RunningStateDetailRepository detailRepository, AlgorithmConfigRepository configRepository, AlarmStatisticsProvider statisticsProvider) {
        this.stateRepository = stateRepository;
        this.detailRepository = detailRepository;
        this.configRepository = configRepository;
        this.statisticsProvider = statisticsProvider;
    }

    /**
     * （*）
     *
     * @param liftNo
     * @param year
     * @return
     */
    public List<LiftRunningState> query(String liftNo, int year) {

        return stateRepository.queryMonthTrends(liftNo, year);
    }

    public LiftRunningState getFromCache(String liftNo,LocalDate scoreDate) {
        return stateRepository.getFromCache(new LiftRunningState(liftNo,scoreDate));
    }

    public List<LiftRunningState> queryBetweenMonths(String liftNo, String beginMonth, String endMonth) {
        return stateRepository.queryBetweenMonths(liftNo, beginMonth, endMonth);
    }

    public List<LiftRunningState> queryBetweenDates(String liftNo, String beginDate, String endDate) {
        return stateRepository.queryDateTrends(liftNo, beginDate, endDate);
    }

    public List<LiftRunningState> queryByLiftNosBetweenDate(List<String> liftNos, String beginDate, String endDate) {
        return stateRepository.queryByLiftNosBetweenDates(liftNos, beginDate, endDate);
    }

    /**
     * （*）
     *
     * @param liftNo
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<RunningStateDetail> queryDetails(String liftNo, String beginDate, String endDate) {
        return detailRepository.queryDetails(liftNo, beginDate, endDate);
    }

    public List<LiftRunningState> queryLatest(List<String> liftNos) {
        LocalDate yesterday = LocalDate.now().minusDays(1);

        return stateRepository.queryByLiftNosAndDate(liftNos, yesterday);
    }

    public void compute(LiftInfo lift, String alarmTypes, LocalDate beginDate, LocalDate endDate) {
        if (beginDate.isAfter(endDate)) return;

        long timestamp = System.currentTimeMillis();

        if (lift.getOnlineTime().isAfter(beginDate)) beginDate = lift.getOnlineTime();

        Table<LocalDate, String, AlarmTypeStatistics> table = statisticsProvider.fetchStatisticsBetweenDates(lift.getLiftNo(), alarmTypes, beginDate, endDate);
        AlgorithmConfig config = configRepository.findAllConfig("00");

//        long duration = ChronoUnit.DAYS.between(beginDate, endDate);

        List<LiftRunningState> states = Lists.newArrayList();
        while (beginDate.isBefore(endDate)) {
            if (log.isDebugEnabled())
                log.debug("{}:{}:{} start compute with alarmTypes : {}", lift.getLiftNo(), beginDate, lift.getYtStatus(), alarmTypes);

            LiftRunningState state = stateRepository.findLatestStateOfScoreDate(lift.getLiftNo(), beginDate.minusDays(1));

            state = state.builderNew(beginDate).compute(
                    AlarmTypeStatistics.buildWithAlarmTypes(
                            alarmTypes,
                            table.containsRow(beginDate) ?
                                    table.row(beginDate).values() :
                                    Collections.EMPTY_LIST),
                    config);

            stateRepository.refreshCache(state);
            states.add(state);

            beginDate = beginDate.plusDays(1);
        }

        stateRepository.saveBatch(states);

        log.info("{} compute Cost:{}, endDate:{},State Size:{}",
                lift.getLiftNo(), System.currentTimeMillis() - timestamp, endDate, states.size());

    }


    public void compute(LiftInfo lift, LocalDate date, String alarmTypes) {
        if (log.isDebugEnabled())
            log.debug("{}:{}:{} start compute with alarmTypes : {}", lift.getLiftNo(), date, lift.getYtStatus(), alarmTypes);
        if (!"20".equals(lift.getYtStatus())) {
            restoreStateOfYesterday(lift.getLiftNo(), date);
        } else {
            List<AlarmTypeStatistics> statistics = statisticsProvider.fetchStatistics(lift.getLiftNo(), alarmTypes, date);

            LiftRunningState state = stateRepository.findLatestStateOfScoreDate(lift.getLiftNo(), date.minusDays(1));

            AlgorithmConfig config = configRepository.findAllConfig("00");

            state = state.builderNew(date).compute(AlarmTypeStatistics.buildWithAlarmTypes(alarmTypes, statistics), config);

            stateRepository.save(state);
        }

    }

    private void restoreStateOfYesterday(String liftNo, LocalDate date) {
        stateRepository.insertStateOfYesterday(liftNo, date);

    }

    public void deleteState(List<String> liftNos, LocalDate beginDate) {
        stateRepository.deleteState(liftNos, beginDate);
    }
}
