package com.xzl.rs.engine.service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xzl.rs.acl.pl.LiftInfo;
import com.xzl.rs.acl.provider.LiftProvider;
import com.xzl.rs.engine.model.AlgorithmConfig;
import com.xzl.rs.engine.model.LiftRunningState;
import com.xzl.rs.engine.repository.AlgorithmConfigRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.IntStream;

@Slf4j
@Service
public class CalculateService {

    final private AlgorithmConfigRepository configRepository;
    final private LiftProvider liftProvider;
    final private LiftRunningStateService stateService;
    final private ThreadPoolTaskExecutor executor;

    public CalculateService(AlgorithmConfigRepository configRepository, LiftProvider liftProvider, LiftRunningStateService stateService, ThreadPoolTaskExecutor executor) {
        this.configRepository = configRepository;
        this.liftProvider = liftProvider;
        this.stateService = stateService;
        this.executor = executor;

    }

    private static final AtomicBoolean MANUAL_MODEL_RUNNING = new AtomicBoolean(false);

    public Boolean get() {
        return MANUAL_MODEL_RUNNING.get();
    }

    public Boolean set(Boolean status) {
        return MANUAL_MODEL_RUNNING.getAndSet(status);
    }

    public Map<String, Object> taskPoolStatus() {
        Map<String, Object> map = Maps.newHashMap();
        map.put("MaxPoolSize", executor.getMaxPoolSize());
        map.put("QueueSize", executor.getThreadPoolExecutor().getQueue().size());
        map.put("ActiveCount", executor.getActiveCount());
        map.put("TaskCount", executor.getThreadPoolExecutor().getTaskCount());
        map.put("CompletedTaskCount", executor.getThreadPoolExecutor().getCompletedTaskCount());
        return map;
    }

    public AlgorithmConfig refreshConfig(String deptNo) {
        configRepository.refreshCache(deptNo);

        return configRepository.findAllConfig(deptNo);
    }

    public LiftRunningState getFromCache(String liftNo, String scoreDate) {
        return stateService.getFromCache(liftNo, LocalDate.parse(scoreDate, DateTimeFormatter.ofPattern("yyyy-MM-dd")));
    }

    public void calculate() {
        LocalDate now = LocalDate.now();
        if (MANUAL_MODEL_RUNNING.get()) {
            log.warn("waiting for calculating with manual model---- {}", now);
            return;
        }

        calculateWithDate(now.minusDays(1));
    }

    public void compute(List<String> liftNos, LocalDate beginDate, LocalDate endDate, Integer type) {
        if (Objects.isNull(liftNos) || liftNos.isEmpty()) {

            if (type.intValue() == 1) {
                doCalculateBasedOnLiftId(beginDate, endDate);
            } else {
                calculate(beginDate, endDate);
            }
        } else {

            List<LiftInfo> lifts = liftProvider.fetchListByLiftNo(liftNos);
            String alarmTypes = configRepository.findAllAlarmTypes("00");

            calculateNow(beginDate, endDate, alarmTypes, lifts);

        }
    }

    public void recompute(List<String> liftNos, LocalDate beginDate, Integer type) {
        stateService.deleteState(liftNos, beginDate);

        compute(liftNos, beginDate, LocalDate.now(), type);
    }

    public void computeWithOffset(Integer offset, Integer limit, LocalDate beginDate, LocalDate endDate) {
        String alarmTypes = configRepository.findAllAlarmTypes("00");
        executor.execute(() -> doCalculate(beginDate, endDate, alarmTypes, offset, limit));
    }


    int limit = 2000;

    public void calculate(LocalDate beginDate, LocalDate endDate) {

        if (beginDate.isAfter(endDate)) {
            log.warn("{} is After {}", beginDate, endDate);
            return;
        }

        List<List<Integer>> groups = computeGroup(endDate, limit);

        if (Objects.isNull(groups)) return;

        String alarmTypes = configRepository.findAllAlarmTypes("00");

//        doCalculate(beginDate, endDate, alarmTypes, 0, 10); // just for test unit
        groups.forEach(group -> {
            group.stream().forEach(offset -> executor.execute(() -> doCalculate(beginDate, endDate, alarmTypes, offset, limit)));
        });
    }

    private void calculateWithDate(LocalDate date) {
        String alarmTypes = configRepository.findAllAlarmTypes("00");

        List<List<Integer>> groups = computeGroup(date, limit);

        if (Objects.isNull(groups)) return;

//        doCalculate(date, alarmTypes, 0, 10); // just for test unit
        groups.forEach(group -> {
            executor.execute(() -> {
                group.stream().forEach(offset -> doCalculate(date, alarmTypes, offset, limit));
            });
        });
    }

    public void doCalculate(LocalDate date, String alarmTypes, int offset, int limit) {
        List<LiftInfo> lifts = liftProvider.fetchLiftsNeedCalculate(date, offset, limit);
        if (Objects.isNull(lifts) || lifts.isEmpty()) return;

        if (log.isDebugEnabled()) log.debug("do Calculate : {},{},{},{}", date, offset, limit, lifts.size());
        lifts.stream().forEach(lift -> {
            try {
                stateService.compute(lift, date, alarmTypes);
            } catch (Exception e) {
                log.error(String.format("%s:%s with %s", lift.getLiftNo(), date, alarmTypes), e);
            }
        });

    }

    //TODO Optimize
    private List<List<Integer>> computeGroup(LocalDate beginDate, int limit) {
        Map<String, Integer> countMap = liftProvider.count(beginDate);
        int count = countMap.get("maxId");
        int trueCount = countMap.get("idCount");
        if (Objects.isNull(count) || 0 == count) return null;

        int round = count / limit;

        int reminder = count % limit;
        if (reminder != 0) round = round + 1;

        List<Integer> offsets = IntStream.range(0, round).map(i -> limit * i).boxed().toList();

        log.warn("{} - lift count:{}, maxId:{} , partition : {}", LocalDate.now(), trueCount, count, offsets);

        return Lists.partition(offsets, executor.getMaxPoolSize());
    }

    private void doCalculate(LocalDate beginDate, LocalDate endDate, String alarmTypes, int offset, int limit) {
        List<LiftInfo> lifts = liftProvider.fetchLiftsNeedCalculate(endDate, offset, limit);

        log.warn("{} - do Calculate : {}-{} ,{} ,{} ,{}", LocalDate.now(), beginDate, endDate, offset, limit, lifts.size());

        calculateNow(beginDate, endDate, alarmTypes, lifts);

    }

    public void doCalculateBasedOnLiftId(LocalDate beginDate, LocalDate endDate) {
        if (beginDate.isAfter(endDate)) {
            log.warn("{} is After {}", beginDate, endDate);
            return;
        }
        String alarmTypes = configRepository.findAllAlarmTypes("00");

        int beginId = 1, end = 10000;
        int step = 10000;

        while (true) {
            List<LiftInfo> lifts = liftProvider.fetchLiftsNeedCalculate(beginId, end, endDate);
            log.warn("{} - doCalculateBasedOnLiftId size:{} ,id range :{}-{}", LocalDate.now(), Objects.isNull(lifts) ? 0 : lifts.size(), beginId, step);

            if (Objects.isNull(lifts) || lifts.isEmpty()) break;


            executor.execute(() -> calculateNow(beginDate, endDate, alarmTypes, lifts));

            beginId += step;
            end += step;


        }
    }


    private void calculateNow(LocalDate beginDate, LocalDate endDate, String alarmTypes, List<LiftInfo> lifts) {
        lifts.stream().forEach(lift -> {
            try {
                stateService.compute(lift, alarmTypes, beginDate, endDate);
            } catch (Exception e) {
                log.error(String.format("%s:%s-%s with %s", lift.getLiftNo(), beginDate, endDate, alarmTypes), e);
            }
        });

    }
}
