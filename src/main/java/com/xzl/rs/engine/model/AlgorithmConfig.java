package com.xzl.rs.engine.model;

import com.google.common.base.Joiner;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import org.apache.logging.log4j.util.Strings;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Data
public class AlgorithmConfig {
    private String id;
    private String deptNo; // 单位 （维保/监管）
    private String alarmType; // 告警类型

    private Integer reduction; // 基础扣分
    private Integer reductionTimes; // 最多扣分次数
    private Double reductionRate; // 扣分速率

    private Integer recoverPeriod; // 分数恢复周期

    public static AlgorithmConfig NULL_CONFIG = new AlgorithmConfig();

    @Getter(AccessLevel.PRIVATE)
    private Map<String, AlgorithmConfig> configMap;

    public AlgorithmConfig() {
    }

    public AlgorithmConfig(List<AlgorithmConfig> configs) {
        if (Objects.isNull(configs) || configs.isEmpty()) return;

        configMap = configs.stream().collect(Collectors.toMap(AlgorithmConfig::getAlarmType, c -> c));
    }

    public boolean isEmpty() {
        return Strings.isEmpty(id) || id.isEmpty() || Objects.isNull(alarmType) || alarmType.isEmpty();
    }

    public AlgorithmConfig getConfigByAlarmType(String alarmType) {
        return configMap.get(alarmType);
    }

    public String getConfigAlarmTypes() {
        if (Objects.isNull(configMap) || configMap.keySet().isEmpty()) return "";
        return Joiner.on(",").join(configMap.keySet());
    }

}
