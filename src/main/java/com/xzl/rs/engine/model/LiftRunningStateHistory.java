package com.xzl.rs.engine.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Getter
@RequiredArgsConstructor(staticName = "of")
class LiftRunningStateHistory {

    @NonNull
    public String liftNo;
    @NonNull
    private LocalDate scoreDate;
    private Double reduceScores;
    private Double recoverScores;
    private List<RunningStateDetail> details;

    private Map<String, RunningStateDetail> detailMap = new HashMap<>();


    public LiftRunningStateHistory buildDetails(List<RunningStateDetail> details) {

        if (Objects.isNull(details) || details.isEmpty()) {
            this.recoverScores = 0.0;
            this.reduceScores = 0.0;
        } else {
            this.details = details;
            details.forEach(rshd -> detailMap.put(rshd.getAlarmType(), rshd));

            computeScores();
        }
        return this;
    }

    public RunningStateDetail fetchOrCreate(String liftNo, String alarmType, Integer alarmCount) {
        if (detailMap.containsKey(alarmType)) return detailMap.get(alarmType);

        return RunningStateDetail.init(liftNo, alarmType, alarmCount);
    }

    private void computeScores() {

        var recoverScores = details.stream().filter(detail -> detail.getScores() > 0)
                .mapToDouble(detail -> BigDecimal.valueOf(detail.getScores()).doubleValue()).sum();
        var reduceScores = details.stream().filter(detail -> detail.getScores() < 0)
                .mapToDouble(detail -> BigDecimal.valueOf(detail.getScores()).doubleValue()).sum();

        this.recoverScores = recoverScores;
        this.reduceScores = reduceScores;

    }

    @Override
    public String toString() {
        return "LiftRunningStateHistory{" +
                "details=" + details +
                '}';
    }
}
