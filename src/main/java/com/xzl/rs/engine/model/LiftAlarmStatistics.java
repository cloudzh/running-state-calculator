package com.xzl.rs.engine.model;

import lombok.Data;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Data
public class LiftAlarmStatistics {

    private String liftNo;
    private String deptNo;
    private LocalDate statisticsDate;

    private String alarmType;
    private Integer alarmCount;

    private Map<String, Integer> alarmCountMap = new HashMap<>();

    public LiftAlarmStatistics init(List<LiftAlarmStatistics> alarmStatistics) {
        alarmStatistics.forEach(s -> alarmCountMap.put(s.getAlarmType(), s.getAlarmCount()));
        return this;
    }


    public Integer getAlarmCount(String alarmType) {
        return alarmCountMap.containsKey(alarmType) ? alarmCountMap.get(alarmType) : 0;
    }
}