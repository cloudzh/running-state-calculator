package com.xzl.rs.engine.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.xzl.rs.ohs.pl.AlarmTypeStatistics;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class LiftRunningState {

    @JsonIgnore
    private String id;

    @NonNull
    private String liftNo;
    @NonNull
    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, locale = "GTM+8")
    private LocalDate scoreDate; //评分日期
    //    @NonNull
    @JsonIgnore
    private AlgorithmConfig algorithmConfig;

    private Double lastScore; // 上次积分
    private Double currentScore; // 当前分数
    private Double reduceScores; // 本次扣分
    private Double recoverScores; // 本次加分
    private Integer riskLevel;// 风险等级

    @JsonIgnore
    private RunningStateDetail currentDetail;

    @JsonIgnore
    private LiftRunningStateHistory lastHistory;

    private static String levelScope = "60,90";

    public void setLastScore(Double lastScore) {
        this.lastScore = scale(lastScore,4);
    }

    public void setCurrentScore(Double currentScore) {
        if (currentScore > 100) {
            this.currentScore = 100.0;
        } else {
            this.currentScore = scale(currentScore,4);
        }
    }

    public void setReduceScores(Double reduceScores) {
        this.reduceScores = scale(reduceScores,4);
    }

    public void setRecoverScores(Double recoverScores) {
        this.recoverScores = scale(recoverScores,4);
    }

    public static void setLevelScope(String levelScope) {
        if (Strings.isNullOrEmpty(levelScope)) return;
        LiftRunningState.levelScope = levelScope;
    }

    private Double scale(Double value,Integer scale) {
        return new BigDecimal(value).setScale(scale, RoundingMode.HALF_UP).doubleValue();
    }

    @JsonIgnore
    public List<RunningStateDetail> getDetails() {
        return lastHistory.getDetails();
    }


    public static LiftRunningState createDefault(String liftNo, LocalDate scoreDate) {
        LiftRunningState state = new LiftRunningState(liftNo, scoreDate);
        state.setId(UUID.randomUUID().toString());
        state.setReduceScores(0.0);
        state.setCurrentScore(100.0);
        state.setLastScore(100.0);
        state.setRecoverScores(0.0);
        state.setLastHistory(LiftRunningStateHistory.of(liftNo, scoreDate));

        return state;
    }

    public LiftRunningState initHistory(List<RunningStateDetail> details) {
        this.lastHistory = LiftRunningStateHistory.of(getLiftNo(), getScoreDate()).buildDetails(details);

        return this;
    }

    public LiftRunningState builderNew(LocalDate newDate) {
        LiftRunningState state = new LiftRunningState();
        state.setId(UUID.randomUUID().toString());
        state.setLiftNo(this.getLiftNo());
        state.setScoreDate(newDate);
        state.setLastScore(this.getCurrentScore());
        state.setCurrentScore(this.getCurrentScore());
        state.setLastHistory(this.getLastHistory());

        return state;
    }

    public LiftRunningState compute(Collection<AlarmTypeStatistics> statistics, AlgorithmConfig config) {

        LiftRunningStateHistory lrsh = LiftRunningStateHistory.of(getLiftNo(), getScoreDate());
        List<RunningStateDetail> details = Lists.newArrayList();

        if (!Objects.isNull(statistics) && !statistics.isEmpty()) {
            statistics.stream().filter(ats -> !Objects.isNull(ats)).forEach(ats -> {
                Optional<RunningStateDetail> optional =
                        doCompute(ats.getAlarmTypeCount(), config.getConfigByAlarmType(ats.getAlarmType()), lastHistory.fetchOrCreate(getLiftNo(), ats.getAlarmType(), ats.getAlarmTypeCount()));

                optional.ifPresent(details::add);
            });

        }

        lrsh.buildDetails(details);

        this.currentScore = getLastScore() + lrsh.getRecoverScores() + lrsh.getReduceScores();
        this.recoverScores = lrsh.getRecoverScores();
        this.reduceScores = lrsh.getReduceScores();

        this.lastHistory = lrsh;
        return this;
    }

    private Optional<RunningStateDetail> doCompute(Integer alarmCount, AlgorithmConfig config, RunningStateDetail latestDetail
    ) {
        int durationDays = 1;
        Double score = 0.0;
        Double moreScore = 0.0;
        if (alarmCount > 0) { // reduce
            Integer basicReduction = config.getReduction();
            Integer times = config.getReductionTimes() <= alarmCount ? config.getReductionTimes() : alarmCount;
            Double rate = config.getReductionRate();
            if (latestDetail.getScores() < 0) {
                // 上次为扣分时，持续时间+1；
                durationDays = latestDetail.getDurationDays() + 1;
            }

            score = (basicReduction * times) + rate * (durationDays - 1) ; //TODO 算法，后续考虑重构，支持多种算法逻辑；

            setReduceScores(score);
            setCurrentScore(getLastScore() - score);

            score = -score;

        } else { // recover
            // last time nothing or recover period reached
            if (latestDetail.getScores().doubleValue() == 0.0 ||
                    (config.getRecoverPeriod().intValue() <= latestDetail.getDurationDays().intValue()))
                return Optional.empty();

            if (latestDetail.getScores() > 0) {// last time recover
                durationDays = latestDetail.getDurationDays() + 1;
                score = latestDetail.getScores();
            } else { // last time reduce

                score = new BigDecimal(latestDetail.getTsid() / config.getRecoverPeriod()).setScale(4, RoundingMode.HALF_UP).doubleValue();
            }

            setRecoverScores(score);
            setCurrentScore(getLastScore() + score);
        }


        RunningStateDetail value = latestDetail.newDetail(getId(), alarmCount, durationDays,config.getRecoverPeriod(), score);
        value.setRecordDate(getScoreDate());
        log.info("{}:{} StateDetail = alarm:{}-{}, durationDays:{},score:{}", value.getLiftNo(), value.getRecordDate(), value.getAlarmType(), value.getAlarmNum(), durationDays, score);
        return Optional.ofNullable(value);
    }


    public Integer getRiskLevel() {
        List<Double> list = Splitter.on(",").splitToStream(levelScope).map(Double::valueOf).collect(Collectors.toList());
        list.add(getCurrentScore());

        list.sort((a, b) -> a > b ? -1 : 0);

        return list.indexOf(getCurrentScore()) + 1;

    }


    @Override
    public String toString() {
        return liftNo + ":" + scoreDate + "=>{" +
                "id='" + id + "\'" +
                ", lastScore=" + lastScore +
                ", currentScore=" + currentScore +
                ", reduceScores=" + reduceScores +
                ", recoverScores=" + recoverScores +
                ", lastHistory=" + lastHistory +
                '}';
    }
}
