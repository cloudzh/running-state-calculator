package com.xzl.rs.engine.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;


@Data
@NoArgsConstructor
public class RunningStateDetail {

    private String stateId;
    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, locale = "GTM+8")
    private LocalDate recordDate;
    @NonNull
    private String liftNo; // 电梯编码
    @NonNull
    private String alarmType; // 告警类型
    @NonNull
    private Integer alarmNum; // 告警数量
    private Integer durationDays; // 持续天数
    private Double scores; // 分数 扣分=负 ； 加分= 正
    private Double tsid; // total scores in durations // 持续时间内的分数和

    public RunningStateDetail(@NonNull String liftNo, @NonNull String alarmType, @NonNull Integer alarmNum) {
        this.liftNo = liftNo;
        this.alarmType = alarmType;
        this.alarmNum = alarmNum;
    }

    public void setScores(Double scores) {
        this.scores = scale(scores, 4);
    }

    public void setTsid(Double tsid) {
        this.tsid = scale(tsid, 4);
    }

    private Double scale(Double value, Integer scale) {
        return new BigDecimal(value).setScale(scale, RoundingMode.HALF_UP).doubleValue();
    }

    /**
     * 初始化，所有属性都有默认值。
     *
     * @param liftNo
     * @param alarmType
     * @param alarmNum
     * @return
     */
    public static RunningStateDetail init(String liftNo, String alarmType, Integer alarmNum) {
        RunningStateDetail detail = new RunningStateDetail();
        detail.setLiftNo(liftNo);
        detail.setAlarmType(alarmType);
        detail.setAlarmNum(alarmNum);
        detail.setDurationDays(0);
        detail.setScores(0.0);
        detail.setTsid(0.0);
        return detail;
    }

    public RunningStateDetail newDetail(String stateId, Integer alarmNum, Integer durationDays, Integer recoverPeriod, Double scores) {
        RunningStateDetail detail = new RunningStateDetail(getLiftNo(), getAlarmType(), alarmNum);
        detail.stateId = stateId;
        detail.durationDays = durationDays;
        detail.scores = scores;
        detail.tsid = getTsid();

        Double moreScore = 0.0;

        if (this.getScores().doubleValue() > 0.0 && scores < 0) {
            int remain = recoverPeriod - this.getDurationDays().intValue();

            if (remain > 0) moreScore = remain * this.getScores();
        }
        detail.handleTSID(scores - moreScore);
        return detail;
    }

    private void handleTSID(Double scores) {
        double _tsid = scores > 0 ? scores : -scores;
        if (durationDays > 1) {
            this.tsid += _tsid;
        } else {
            this.tsid = _tsid;
        }
    }

    @Override
    public String toString() {
        return "RunningStateDetail{" +
                "recordDate=" + recordDate +
                ", liftNo='" + liftNo + '\'' +
                ", alarmType='" + alarmType + '\'' +
                ", alarmNum=" + alarmNum +
                ", durationDays=" + durationDays +
                ", scores=" + scores +
                ", tsid=" + tsid +
                '}';
    }
}
