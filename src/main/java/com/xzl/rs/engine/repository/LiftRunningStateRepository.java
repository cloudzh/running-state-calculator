package com.xzl.rs.engine.repository;

import com.xzl.rs.engine.model.LiftRunningState;
import com.xzl.rs.engine.model.RunningStateDetail;
import org.springframework.cglib.core.Local;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public interface LiftRunningStateRepository {

    void save(LiftRunningState state);

    List<LiftRunningState> queryMonthTrends(String liftNo, int year);

    List<LiftRunningState> queryByLiftNosBetweenDates(List<String> liftNos, String beginDate, String endDate);

    List<LiftRunningState> queryBetweenMonths(String liftNo, String beginMonth, String endMonth);

    List<LiftRunningState> queryDateTrends(String liftNo, String beginDate, String endDate);


    LiftRunningState findLatestStateOfScoreDate(String liftNo, LocalDate scoreDate);

    List<LiftRunningState> queryByLiftNosAndDate(List<String> liftNos,LocalDate date);


    void insertStateOfYesterday(String liftNo,LocalDate date);

    void saveBatch(List<LiftRunningState> states);

    void refreshCache(LiftRunningState state);


    LiftRunningState getFromCache(LiftRunningState state);

    void deleteState(List<String> liftNos, LocalDate beginDate);

    @Scheduled(fixedDelay = 6, timeUnit = TimeUnit.SECONDS)
    void doBatchInsertToClickHouse();

    @Scheduled(fixedRate = 10, timeUnit = TimeUnit.SECONDS)
    void doBatchInsertToClickHouseJob2();
}
