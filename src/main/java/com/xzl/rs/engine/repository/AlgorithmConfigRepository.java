package com.xzl.rs.engine.repository;

import com.xzl.rs.engine.model.AlgorithmConfig;

public interface AlgorithmConfigRepository {

    AlgorithmConfig findConfig(String alarmType,String deptNo);

    void updateConfig(AlgorithmConfig config);

    String findAllAlarmTypes(String deptNo);

    AlgorithmConfig findAllConfig(String deptNo);

    void save(AlgorithmConfig config);

    void refreshCache(String deptNo);
}
