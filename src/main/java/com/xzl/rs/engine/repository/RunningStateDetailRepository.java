package com.xzl.rs.engine.repository;

import com.xzl.rs.engine.model.RunningStateDetail;

import java.util.List;

public interface RunningStateDetailRepository {

    List<RunningStateDetail> queryDetails(String liftNo, String beginDate, String endDate);

}
