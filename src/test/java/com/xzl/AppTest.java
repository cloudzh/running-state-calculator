package com.xzl;


import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.xzl.rs.ohs.pl.AlarmTypeStatistics;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.List;
import java.util.random.RandomGenerator;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {
        assertTrue(true);
    }

    @Test
    public void test_division() {
        System.out.println(1 / 3.0f);
    }

    @Test
    public void test_calendar() {
        Calendar calendar = Calendar.getInstance();

        System.out.println(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        System.out.println(calendar.getActualMinimum(Calendar.DAY_OF_MONTH));

        calendar.set(Calendar.YEAR, 2019);
        calendar.set(Calendar.MONTH, 2);
        calendar.set(Calendar.DAY_OF_MONTH, 1);


        System.out.println(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        System.out.println(calendar.getActualMinimum(Calendar.DAY_OF_MONTH));

        calendar.set(2019, 2, 10);

        System.out.println(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        System.out.println(calendar.getActualMinimum(Calendar.DAY_OF_MONTH));

        LocalDate date = LocalDate.now();

        System.out.println(date.getMonth().maxLength());

        date = date.minusMonths(10);

        System.out.println(date.getMonth().maxLength() + ":" + date.getMonthValue());

    }

    @Test
    public void test_IntStream_of() {
        IntStream.of(1, 10).forEach(System.out::print); //1,10
        IntStream.range(1, 10).forEach(System.out::print);// 1-9
        IntStream.rangeClosed(1, 10).forEach(System.out::print);//1-10

        List<Integer> a = Lists.newArrayList();
        IntStream.rangeClosed(1, 10).map(i -> i * 1000).forEach(a::add);
        System.out.println(a);

        List<Integer> is = IntStream.rangeClosed(1, 10).map(i -> i * 10).boxed().toList();

        a.stream().map(i -> i * 10).toList();

    }

    @Test
    public void test_month() {
        YearMonth ym1 = YearMonth.of(2021, 2);

        System.out.println(ym1.lengthOfMonth());

        System.out.println(ym1.getMonthValue());


        System.out.println(RandomGenerator.getDefault().nextDouble(0.0, 100.0));
        System.out.println(RandomGenerator.getDefault().nextDouble(0.0, 100.0));
        System.out.println(RandomGenerator.getDefault().nextDouble(0.0, 100.0));
        System.out.println(RandomGenerator.getDefault().nextDouble(0.0, 100.0));
        System.out.println(RandomGenerator.getDefault().nextDouble(0.0, 100.0));

//        Calendar calendar = Calendar
    }


    @Test
    public void test_double_scala(){
        System.out.println(new BigDecimal(1.32233223).setScale(2, RoundingMode.HALF_UP));
        System.out.println(new BigDecimal(1.32533223).setScale(2, RoundingMode.HALF_UP));
    }

    @Test
    public void test_duration_between_dates() {
        LocalDate bd = LocalDate.of(2021, 1, 1);
        LocalDate ed = LocalDate.of(2021, 1, 3);
//        System.out.println(Duration.between(bd, ed).toDays());

        System.out.println(ChronoUnit.DAYS.between(bd, ed));
        System.out.println(ChronoUnit.DAYS.between(ed, bd));

        Double a = 0.1;

        System.out.println(a>0);
        System.out.println(a.intValue()>0);
    }

    @Test
    public void test_table() {
        Table<String,String,String> table = HashBasedTable.create();

        table.put("a","a","a");
        table.put("a","a","a1");

        System.out.println(table.get("a", "a"));
    }
}
