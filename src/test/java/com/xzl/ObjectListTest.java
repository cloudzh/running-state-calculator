package com.xzl;

import com.google.common.collect.Lists;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.IntStream;

public class ObjectListTest {

    @Test
    public void test() {
        List<Double> list = Lists.newArrayList(20.0, 40.0, 60.0, 80.0);
        list.add(80.0);
        list.sort((a, b) -> a > b ? 0 : -1);

        System.out.printf("%s,%s", list, list.indexOf(80.0));

        List<Double> l2 = Lists.newArrayList(20.0, 40.0, 60.0, 80.0);

        l2.add(20.0);
        l2.sort((a, b) -> a > b ? 0 : -1);

        System.out.printf("%s,%s", l2, l2.indexOf(20.0));
    }

    @Test
    public void test_IntStream_rangeClose() {

        IntStream.rangeClosed(0,10).forEach(System.out::println);
        IntStream.range(0,10).forEach(System.out::println);
    }


}
