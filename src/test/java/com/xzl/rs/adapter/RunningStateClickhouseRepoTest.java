package com.xzl.rs.adapter;

import com.xzl.rs.acl.adapter.RunningStateClickhouseRepo;
import com.xzl.rs.engine.model.LiftRunningState;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import java.util.random.RandomGenerator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


@Slf4j
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, properties = {"spring.cloud.consul.enabled=false"})
@TestPropertySource("classpath:application-clickhouse.properties")
class RunningStateClickhouseRepoTest {

    @Resource
    private RunningStateClickhouseRepo stateRepo;

    @Test
    void save() {
        LiftRunningState state = new LiftRunningState();
        state.setId(UUID.randomUUID().toString());
        state.setLiftNo("3303-3333-3333");
        state.setScoreDate(LocalDate.now());
        state.setLastScore(10.0);
        state.setCurrentScore(90.0);
        state.setRecoverScores(80.0);
        state.setReduceScores(0.0);

        stateRepo.save(state);
    }

    @Test
    void save_batch() {
        RandomGenerator rg = RandomGenerator.getDefault();
        List<LiftRunningState> states = IntStream.range(0, 10).mapToObj(i -> {
            LiftRunningState state = new LiftRunningState();
            state.setId(UUID.randomUUID().toString());
            state.setLiftNo("3303-3333-" + rg.nextInt(1000, 9999));
            state.setScoreDate(LocalDate.now());
            state.setLastScore(rg.nextDouble(0, 100));
            state.setCurrentScore(rg.nextDouble(0, 100));
            state.setRecoverScores(rg.nextDouble(0, 30));
            state.setReduceScores(rg.nextDouble(0, 30));
            return state;
        }).collect(Collectors.toList());

        stateRepo.saveNow(states);
    }


    @Test
    void query() {
        LiftRunningState state = stateRepo.query("330108002-0017-0108", LocalDate.of(2021, 10, 26));
        log.info("{}", state);
    }

    @Test
    void queryList() {
        List<LiftRunningState> states = stateRepo.query("330108002-0017-0108", Lists.newArrayList(LocalDate.of(2021, 10, 26), LocalDate.of(2021, 10, 27)));

        log.info("{}", states);
    }

    @Test
    void queryByLiftNosAndDate() {
        List<LiftRunningState> states = stateRepo.queryByLiftNosAndDate(Lists.newArrayList("330108002-0017-0108", "330108002-0015-0107"), LocalDate.of(2021, 10, 26));
        log.info("{}", states);
    }

    @Test
    void queryByLiftNoAndBetweenDate() {
        List<LiftRunningState> states = stateRepo.queryByLiftNoAndBetweenDate("330108002-0017-0108", LocalDate.of(2021, 10, 26), LocalDate.of(2021, 10, 27));
        log.info("{}", states);
    }
}