package com.xzl.rs.adapter;

import com.xzl.rs.acl.adapter.AlgorithmConfigClickHouseRepo;
import com.xzl.rs.engine.model.AlgorithmConfig;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

import javax.annotation.Resource;

import java.util.List;
import java.util.Map;

@Slf4j
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@TestPropertySource("classpath:application-clickhouse.properties")
class AlgorithmConfigClickHouseRepoTest {

    @Resource
    private AlgorithmConfigClickHouseRepo repo;
    @Resource
    private JdbcTemplate jdbcTemplate;

    @Test
    void queryByDept() {
        List<AlgorithmConfig> configList = repo.queryByDeptNo("00");

        log.info("result: {},{}",configList.size(), configList);

    }

    @Test
    void queryByDeptAndAlarmType() {

        AlgorithmConfig config = repo.queryByDeptAndAlarmType("00", "1000007");

        log.info("AlgorithmConfig: {}", config);
    }

    @Test
    void test_origin_sql() {

        List<Map<String, Object>> maps = jdbcTemplate.queryForList("select * from algorithm_config");
        log.info("result : {}", maps.isEmpty());
    }
}