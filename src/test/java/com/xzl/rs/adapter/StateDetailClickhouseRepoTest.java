package com.xzl.rs.adapter;

import com.xzl.rs.acl.adapter.StateDetailClickhouseRepo;
import com.xzl.rs.engine.model.RunningStateDetail;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.TestPropertySource;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import java.util.random.RandomGenerator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, properties = {"spring.cloud.consul.enabled=false"})
@TestPropertySource("classpath:application-clickhouse.properties")
class StateDetailClickhouseRepoTest {

    @Resource
    private JdbcTemplate jdbcTemplate;

    @Resource
    private StateDetailClickhouseRepo detailRepo;


    @Test
    void query() {
    }

    @Test
    void queryForList() {
    }

    @Test
    void queryByLiftNoAndBetweenDate() {
    }

    @Test
    void insertBatch() {
        RandomGenerator def = RandomGenerator.getDefault();
        List<RunningStateDetail> details = IntStream.range(0,10).mapToObj(i -> {
            RunningStateDetail detail = new RunningStateDetail();
            detail.setStateId(UUID.randomUUID().toString());
            detail.setScores(def.nextDouble(-50, 50));
            detail.setRecordDate(LocalDate.now());
            detail.setAlarmType("1000010");
            detail.setAlarmNum(10);
            detail.setDurationDays(1);
            detail.setTsid(def.nextDouble(1, 100));
            detail.setLiftNo("33330-0333-" + def.nextInt(1000, 9000));
            return detail;
        }).collect(Collectors.toList());

        detailRepo.save(details);
    }
}