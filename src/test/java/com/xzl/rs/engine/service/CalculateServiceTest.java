package com.xzl.rs.engine.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.annotation.Resource;
import java.time.LocalDate;


@Slf4j
@Profile("dev")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, properties = {"spring.cloud.consul.enabled=false"})
class CalculateServiceTest {

    @Resource
    private CalculateService calculateService;
    @Resource
    private ThreadPoolTaskExecutor executor;

    @Test
    void calculate() {
        calculateService.calculate(LocalDate.of(2021, 10, 27), LocalDate.of(2021, 11, 1));

        log.warn("active count : {}  ============================", executor.getActiveCount());
        while (executor.getActiveCount() > 0) {
            try {
                Thread.sleep(10*1000);
                log.warn("active count : {}  ============================", executor.getActiveCount());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    void calculate_single() {

    }

}