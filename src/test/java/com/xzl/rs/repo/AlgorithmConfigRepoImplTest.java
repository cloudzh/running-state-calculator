package com.xzl.rs.repo;

import com.xzl.rs.engine.model.AlgorithmConfig;
import com.xzl.rs.engine.repository.AlgorithmConfigRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import javax.annotation.Resource;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@TestPropertySource("classpath:application-clickhouse.properties")
class AlgorithmConfigRepoImplTest {

    @Resource
    private AlgorithmConfigRepository repository;

    @Test
    void findConfig() {
        AlgorithmConfig config = repository.findConfig("00","10000007");
        log.info("AlgorithmConfig 1: {}", config);

        config = repository.findConfig("00","10000007");
        log.info("AlgorithmConfig 2: {}", config);

        config = repository.findConfig("00","10000007");
        log.info("AlgorithmConfig 3: {}", config);
    }
}